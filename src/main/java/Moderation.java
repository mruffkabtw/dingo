import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.Manager;
import net.dv8tion.jda.api.managers.RoleManager;

import java.util.HashMap;
import java.util.List;

public class Moderation extends ListenerAdapter {

    public HashMap<Member, Integer> warns = new HashMap<>();

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(!warns.containsKey(e.getMember())) {
            warns.put(e.getMember(), 0);
        }
        /*
            KICK COMMAND
         */
        if (e.getMessage().getContentRaw().startsWith("d!kick")) {
            Member author = e.getMessage().getMember();
            if (!author.hasPermission(Permission.KICK_MEMBERS)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            }

            List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
            if (mentionedMembers.isEmpty()) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz osobę którą chcesz wyrzucić");
            }

            e.getGuild().kick(mentionedMembers.get(0)).queue(success -> {
                Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie wyrzucono: **" + mentionedMembers.get(0).getUser().getName()+"**!");
            });
        }
        /*
            BAN COMMAND
         */
        if (e.getMessage().getContentRaw().startsWith("d!ban")) {
            Member author = e.getMessage().getMember();
            if (!author.hasPermission(Permission.BAN_MEMBERS)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            }

            List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
            if (mentionedMembers.isEmpty()) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz osobę którą chcesz zbanować");
            }

            e.getGuild().ban(mentionedMembers.get(0), 7).queue(success -> {
                Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie zbanowano: **" + mentionedMembers.get(0).getUser().getName()+"**!");
            });
        }
        /*
            MUTE COMMAND
            TODO MUTE COMMAND
         */
        if(e.getMessage().getContentRaw().startsWith("d!mute")) {
            Member author = e.getMessage().getMember();
            if (!author.hasPermission(Permission.BAN_MEMBERS)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            }

            List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
            if (mentionedMembers.isEmpty()) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz osobę którą chcesz zbanować");
            }


        }


        /*
            WARN SYSTEM
         */
        if(e.getMessage().getContentRaw().startsWith("d!warn")) {
            Member author = e.getMessage().getMember();
            if (!author.hasPermission(Permission.KICK_MEMBERS)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            } else {
                List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
                if (mentionedMembers.isEmpty()) {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz osobę której chcesz dać warna");
                } else {

                    warns.put(mentionedMembers.get(0), (warns.get(mentionedMembers.get(0))) + (1));
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie dodano warna dla: **" + mentionedMembers.get(0).getUser().getName()+"**!");
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  "+mentionedMembers.get(0).getUser().getAsMention()+" posiada "+warns.get(mentionedMembers.get(0))+" warnów");
                    if((warns.get(mentionedMembers.get(0))) >= (3)) {
                        e.getGuild().ban(mentionedMembers.get(0), 7);
                        warns.remove(mentionedMembers.get(0));
                    }
                }
            }
        }
        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!my-warns")) {
            int i = warns.get(e.getMember());
            Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  "+e.getAuthor().getAsMention()+" posiada "+i+" warnów");
        }
        String[] args = e.getMessage().getContentRaw().split(" ", 2);
        if(args[0].equalsIgnoreCase("d!wyczysc")) {
            if(e.getMember().hasPermission(Permission.MANAGE_CHANNEL)) {
                if(args.length == 2) {
                    int id = Integer.parseInt(args[1]);
                    List<Message> msgs = e.getChannel().getHistory().retrievePast(id).complete();
                    int i = 0;
                    for (i = 0; i < msgs.size(); i++) {
                        msgs.get(i).delete().queue();
                    }

                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie usunięto `"+i+"` wiadomości");
                } else {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Podaj ilośc wiadomości do wyczyszczenia");
                }
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            }
        }
        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!token")) {
            if(e.getMember().isOwner() == true) {
                String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                e.getAuthor().openPrivateChannel().queue(channel -> {
                    MessageBuilder mb = new MessageBuilder();
                    mb.appendFormat(">>> Twój token: "+token+"\n Jest on używany przez bazę danych do odczytu danych");
                    channel.sendMessage(mb.build()).queue();
                });
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Musisz być właścicielem serwera!");
            }
        }

    }

}
