import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.net.URL;
import java.util.List;

public class Fun extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!moneta")) {
            int i = (int) (Math.random() * 100) + 1;
            if(i >= 50) {
                Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", " *Wypadła `reszka!`*");
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", " *Wypadł `orzełek!`*");
            }
        }
        String[] args = e.getMessage().getContentRaw().split(" ", 2);
        if(args[0].equalsIgnoreCase("d!lovecalc")) {
            if(args.length == 2) {
                if(args[1].length() > 20) {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "Maksymalnie `20` znaków!");
                } else {
                    int i = args[1].length();
                    int percent = i+(i * 10 / (2 * -i));

                    int per =  (int) (Math.random() * 100) + 1;
                    Main.sendBuildedMessageID(e.getChannel(), "734056852446380074", "Poziom miłości między "+e.getAuthor().getAsMention()+" a `"+args[1]+"` wynosi: `"+per+"`");
                }
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "Musisz kogoś podać!");
            }
        }
        if(e.getMessage().getContentRaw().startsWith("d!avatar")) {

            List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
            if (mentionedMembers.isEmpty()) {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734056854790864966").getAsMention()+" Avatar");
                eb.setThumbnail(e.getAuthor().getEffectiveAvatarUrl());
                eb.setColor(new Color(0, 174, 255));
                e.getChannel().sendMessage(eb.build()).queue();
            } else {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734056854790864966").getAsMention() + " Avatar "+mentionedMembers.get(0).getEffectiveName());
                eb.setThumbnail(mentionedMembers.get(0).getUser().getEffectiveAvatarUrl());
                eb.setColor(new Color(0, 174, 255));
                e.getChannel().sendMessage(eb.build()).queue();
            }
        }

    }

}
