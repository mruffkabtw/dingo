import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class WelcomeGoodbay extends ListenerAdapter {

    private String welcomeMessage;
    private TextChannel welcomeChannel;

    private String goodbayMessage;
    private TextChannel goodbayChannel;

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(e.getMessage().getContentRaw().startsWith("d!powitanie-kanal")) {
            if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            } else {
                List<TextChannel> mentionedChannels= e.getMessage().getMentionedChannels();
                if (mentionedChannels.isEmpty()) {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz kanał który chcesz ustawić");
                } else {
                    String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                    DatabaseConnectionFunctions.updateWelcomeMessage("UPDATE "+token+" SET welcomeChannel = "+mentionedChannels.get(0).getId());
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie ustawiono kanał powitalny!");
                }
            }
        }
        if(e.getMessage().getContentRaw().startsWith("d!pozegnanie-kanal")) {
            if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            } else {
                List<TextChannel> mentionedChannels= e.getMessage().getMentionedChannels();
                if (mentionedChannels.isEmpty()) {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Oznacz kanał który chcesz ustawić");
                } else {
                    String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                    DatabaseConnectionFunctions.updateGoodbayMessage("UPDATE "+token+" SET goodbayChannel = "+mentionedChannels.get(0).getId());
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie ustawiono kanał pożegnalny!");
                }
            }
        }



        String[] args = e.getMessage().getContentRaw().split(" ", 2);
        if(args[0].equalsIgnoreCase("d!powitanie")) {
            if(!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            } else {
                if(args.length == 2) {
                    String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                    DatabaseConnectionFunctions.updateWelcomeMessage("UPDATE "+token+" SET welcomeMessage = \""+args[1]+"\"");
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie ustawiono treść wiadomości powitalnej!");
                } else {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Podaj treść wiadomości powitalnej");
                }
            }
        }
        if(args[0].equalsIgnoreCase("d!pozegnanie")) {
            if(!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            } else {
                if(args.length == 2) {
                    String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                    DatabaseConnectionFunctions.updateWelcomeMessage("UPDATE "+token+" SET goodbayMessage = \""+args[1]+"\"");
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie ustawiono treść wiadomości pozegnaj!");
                } else {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Podaj treść wiadomości pozeganlnej");
                }
            }
        }

    }


    public void onGuildMemberJoin(GuildMemberJoinEvent e) {
        String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
        String id = DatabaseConnectionFunctions.getWelcomeChannel(token, "SELECT welcomeChannel FROM "+token+" ");
        String converted = DatabaseConnectionFunctions.getWelcomeMessage(token, "SELECT welcomeMessage FROM "+token+" ").replace("{USER}", e.getMember().getAsMention()).replace("{SERVER_NAME}", e.getGuild().getName());
        e.getGuild().getTextChannelById(id).sendMessage(converted).queue();
    }

    public void onGuildMemberLeave(GuildMemberLeaveEvent e) {
        String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
        String id = DatabaseConnectionFunctions.getGoodbayChannel(token, "SELECT goodbayChannel FROM "+token+" ");
        String converted = DatabaseConnectionFunctions.getGoodbayMessage(token, "SELECT goodbayMessage FROM "+token+" ").replace("{USER}", e.getMember().getAsMention()).replace("{SERVER_NAME}", e.getGuild().getName());
        e.getGuild().getTextChannelById(id).sendMessage(converted).queue();
    }

}
