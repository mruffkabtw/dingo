import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.xml.crypto.Data;
import java.util.List;

public class RoleOnJoin extends ListenerAdapter {

    public void onGuildMemberJoin(GuildMemberJoinEvent e) {
        String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
        e.getGuild().addRoleToMember(e.getMember(), e.getGuild().getRoleById(DatabaseConnectionFunctions.getGoodbayChannel(token, "SELECT onjoinrole FROM "+token+" "))).queue();
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if (e.getMessage().getContentRaw().contains("d!join-role")) {
            if (e.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                List<Role> list = e.getMessage().getMentionedRoles();
                if (!list.isEmpty()) {
                    String token = DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId());
                    DatabaseConnectionFunctions.updateWelcomeChannel("UPDATE "+token+" SET onjoinrole = "+list.get(0).getId());
                    Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Pomyślnie dodano role!");
                } else {
                    Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Podaj role które użytkownik ma dostać po dołączeniu na serwer");
                }
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "  Nie masz uprawnień!");
            }
        }
    }
}
