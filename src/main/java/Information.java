import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

public class Information extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        String[] args = e.getMessage().getContentRaw().split(" ");
        if(args[0].equalsIgnoreCase("d!id")) {
            if(args.length == 2) {
                Member m = e.getGuild().getMemberById(args[1]);
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734395400651276289").getAsMention()+"   Znaleziono użytkownika!");
                eb.setColor(new Color(136, 0, 253));
                eb.setDescription("Użytkownik o podanym ID: **"+m.getEffectiveName()+"**");
                eb.setThumbnail(e.getJDA().getUserById(args[1]).getEffectiveAvatarUrl());
                eb.setFooter("Gdy bot nie odpisuje, znaczy że nie może odnaleźć użytkownika • "+e.getMessage().getTimeCreated().getHour()+":"+e.getMessage().getTimeCreated().getMinute(), e.getAuthor().getAvatarUrl());
                e.getChannel().sendMessage(eb.build()).queue();
            } else {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "Poprawne użycie: `d!id <id_użytkownika>`");
            }


        }
        if(args[0].equalsIgnoreCase("d!server")) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734056854790864966").getAsMention()+"   Informacje o serwerze");
            eb.setColor(new Color(0, 174, 255));
            eb.setFooter("Wiadomości wygenerowane automatycznie • "+e.getMessage().getTimeCreated().getHour()+":"+e.getMessage().getTimeCreated().getMinute(), e.getAuthor().getAvatarUrl());
            eb.setDescription("**ID:** "+e.getGuild().getId()
                    +"\n**NAZWA: **"+e.getGuild().getName()
                    +"\n**BOOSTY: **"+e.getGuild().getBoostCount()
                    +"\n**UŻYTKOWNICY: **"+e.getGuild().getMembers().size()
                    +"\n**ROLE: **"+e.getGuild().getRoles().size()
                    +"\n**KANAŁY: **"+e.getGuild().getChannels().size());
            e.getChannel().sendMessage(eb.build()).queue();
        }
        if(e.getMessage().getContentRaw().startsWith("d!user")) {
            List<Member> mentionedMembers = e.getMessage().getMentionedMembers();
            if(mentionedMembers.isEmpty()) {
                Main.sendBuildedMessageID(e.getChannel(), "734367071672074330", "Poprawne użycie: `d!user <oznaczenie_użytkownika>`");
            } else {
                EmbedBuilder eb = new EmbedBuilder();
                eb.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734395400651276289").getAsMention()+"   Znaleziono użytkownika!");
                eb.setColor(new Color(136, 0, 253));
                eb.setDescription("ID: **"+mentionedMembers.get(0).getId()+"**\n"
                        +"NAZWA UŻYTKOWNIKA: **"+mentionedMembers.get(0).getEffectiveName()+"**\n"
                        +"ILOŚĆ ROLI: **"+mentionedMembers.get(0).getRoles().size()+"**\n"
                        +"DATA UTWORZENIA: **"+mentionedMembers.get(0).getTimeCreated().toLocalDateTime().getYear()+"-"+mentionedMembers.get(0).getTimeCreated().toLocalDateTime().getMonthValue()+"-"+mentionedMembers.get(0).getTimeCreated().toLocalDateTime().getDayOfMonth()+"**\n"
                        +"DATA DOŁĄCZENIA: **"+mentionedMembers.get(0).getTimeJoined().toLocalDateTime().getYear()+"-"+mentionedMembers.get(0).getTimeJoined().toLocalDateTime().getMonthValue()+"-"+mentionedMembers.get(0).getTimeJoined().toLocalDateTime().getDayOfMonth()+"**\n"
                );
                eb.setThumbnail(e.getJDA().getUserById(mentionedMembers.get(0).getId()).getEffectiveAvatarUrl());
                eb.setFooter("Gdy bot nie odpisuje, znaczy że nie może odnaleźć użytkownika • "+e.getMessage().getTimeCreated().toLocalTime().getHour()+":"+e.getMessage().getTimeCreated().toLocalTime().getMinute(), e.getAuthor().getAvatarUrl());
                e.getChannel().sendMessage(eb.build()).queue();
            }
        }
        if(args[0].equalsIgnoreCase("d!autor")) {
            e.getAuthor().openPrivateChannel().queue(channel -> {
                MessageBuilder mb = new MessageBuilder();
                mb.appendFormat(">>> "+e.getJDA().getGuildById("720664306043191377").getEmoteById("734666494839095346").getAsMention()+" Autorem bota jest: mruffka.java#3176 i LenVoTV#0453");
                channel.sendMessage(mb.build()).queue();
            });
        }
        int tnl = LevelingSystem.readUserToNextLevel(e.getAuthor().getId());
        int lvl = LevelingSystem.readUserLevel(e.getAuthor().getId());
        int exp = LevelingSystem.readUserExp(e.getAuthor().getId());

        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!level")) {
            Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Twój poziom: `"+lvl+"`");
            Main.sendBuildedMessageID(e.getChannel(), "734367072905068545", "  Do następnego poziomu brakuje Ci: `"+(tnl-exp)+"`");

        }

        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!aktywuj-bota")) {
            if(e.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                if(DatabaseConnectionFunctions.getTokenFromTable(e.getGuild().getId()) != null) {
                    Main.sendBuildedMessageID(e.getChannel(), "734666494839095346", "  Bot został juz aktywowany!");
                } else {
                    int leftLimit = 97; // letter 'a'
                    int rightLimit = 122; // letter 'z'
                    int targetStringLength = 30;
                    Random random = new Random();
                    String generatedString = random.ints(leftLimit, rightLimit + 1)
                            .limit(targetStringLength)
                            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                            .toString();
                    Main.sendBuildedMessageID(e.getChannel(), "734666494839095346", "  Pomyślnie aktywowano bota!");
                    String i = e.getGuild().getSystemChannel().getId();
                    DbConnection.createGuildTable(generatedString);
                    DatabaseConnectionFunctions.updateWelcomeMessage("INSERT INTO "+generatedString+"(onjoinrole) VALUES(000000000000000000) ");
                    DatabaseConnectionFunctions.insertGuildToken(e.getGuild().getId(), generatedString, "INSERT INTO GuildTokens(GuildID, Token) VALUES(?, ?) ");
                    DatabaseConnectionFunctions.insertWelcomeGoodbay(
                            i,
                            "Witaj, {USER} na serwerze **{SERVER_NAME}**!",
                            i,
                            "Żegnaj, {USER}",
                            "INSERT INTO "+generatedString+"(welcomeMessage, welcomeChannel, goodbayMessage, goodbayChannel) VALUES(?, ?, ?, ?) ");
                }
            }
        }
    }

}
