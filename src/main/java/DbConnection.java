import net.dv8tion.jda.api.entities.Guild;

import java.sql.*;

public class DbConnection {

    public static Connection connect() {
        Connection con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:dingoBot.db");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void createGuildTable(String token) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try{
            String sql = "CREATE TABLE \""+token+"\" (\n" +
                    "\t\"welcomeMessage\"\tTEXT,\n" +
                    "\t\"goodbayMessage\"\tTEXT,\n" +
                    "\t\"welcomeChannel\"\tTEXT,\n" +
                    "\t\"goodbayChannel\"\tTEXT,\n" +
                    "\t\"onjoinrole\"\tINTEGER\n" +
                    ");";
            ps = con.prepareStatement(sql);
            ps.execute();

        } catch(SQLException e) {
            System.out.println(e.toString());
        }

    }

}
