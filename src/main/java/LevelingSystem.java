import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class LevelingSystem extends ListenerAdapter {

    public static void insertUser(String ID, int lvl, int exp, int tnl) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try{
            String sql = "INSERT INTO UserLevel(User, level, exp, tonextlevel) VALUES(?, ?, ?, ?) ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            ps.setInt(2, lvl);
            ps.setInt(3, exp);
            ps.setInt(4, tnl);
            ps.execute();

        } catch(SQLException e) {
            System.out.println(e.toString());
        }

    }

    //READING USER LEVEL

    public static int readUserLevel(String ID) {
        int lvl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT level FROM UserLevel WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            rs = ps.executeQuery();

            lvl = rs.getInt(1);

            return lvl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return 0;
    }

    //READING USER EXPIRENCE

    public static int readUserExp(String ID) {
        int exp;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT exp FROM UserLevel WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            rs = ps.executeQuery();

            exp = rs.getInt(1);

            return exp;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return 0;
    }

    //READING USER TONEXTLEVEL

    public static int readUserToNextLevel(String ID) {
        int tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT tonextlevel FROM UserLevel WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            rs = ps.executeQuery();

            tnl = rs.getInt(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return 0;
    }

    public static void updateLevels(String ID, int lvl, int exp, int tnl) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE UserLevel SET level = ? WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setInt(1, lvl);
            ps.setString(2, ID);
            ps.execute();

            sql = "UPDATE UserLevel SET exp = ? WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setInt(1, exp);
            ps.setString(2, ID);
            ps.execute();

            sql = "UPDATE UserLevel SET tonextlevel = ? WHERE User = ? ";
            ps = con.prepareStatement(sql);
            ps.setInt(1, tnl);
            ps.setString(2, ID);
            ps.execute();
        } catch(SQLException e) {
            System.out.println(e.toString());
        }
    }

    public static int getNumberOfUsers(String ID) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select count(User) from UserLevel where User = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            rs = ps.executeQuery();
            int i = rs.getInt(1);
            return i;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                ps.close();
                rs.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return 0;
    }







    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {

        if(!e.getAuthor().isBot()) {
            int expToAdd = (int) (Math.random() * 30) + 10;
            updateLevels(
                    e.getAuthor().getId(),
                    readUserLevel(e.getAuthor().getId()),
                    (readUserExp(e.getAuthor().getId()) + expToAdd),
                    readUserToNextLevel(e.getAuthor().getId()));
            if((readUserExp(e.getAuthor().getId())) >= (readUserToNextLevel(e.getAuthor().getId()))) {
                updateLevels(
                        e.getAuthor().getId(),
                        (readUserLevel(e.getAuthor().getId()) + (1)),
                        (readUserExp(e.getAuthor().getId()) - (readUserToNextLevel(e.getAuthor().getId()))),
                        (readUserToNextLevel(e.getAuthor().getId()) * (2)));

                e.getChannel().sendMessage("masz nowy level kekw").queue(message -> {
                    message.delete().queueAfter(3, TimeUnit.SECONDS);
                });
            }
        }
    }

}
