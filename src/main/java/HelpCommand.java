import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;

public class HelpCommand extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(e.getMessage().getContentRaw().equalsIgnoreCase("d!help")) {
            e.getAuthor().openPrivateChannel().queue(channel -> {
                /*
                    MODERACYJNE
                 */
                EmbedBuilder mod = new EmbedBuilder();
                mod.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734395400651276289").getAsMention()+"   Moderacyjne");
                mod.setColor(new Color(136, 0, 253));
                mod.setFooter(" • Wiadomości wygenerowane automatycznie", e.getAuthor().getAvatarUrl());
                mod.setDescription("`Komendy dla moderacji` \n\n"
                        +" d!kick - wyrzuca użytkownika z serwera\n"
                        +" d!ban - banuje użytkownika\n"
                        +" d!warn - ostrzega użytkownika (3 warn = ban 7dni)\n"
                        +" d!my-warns - wyświetla ilość ostrzeżeń\n"
                        +" d!join-role - ustawia rolę którą użytkownik ma dostać po wejściu na serwer\n"
                        +" d!wyczysc - czysci czat (max 100wiad/komendę)\n"
                        +" d!powitanie-kanal - ustawia kanal powiadomien powitań\n"
                        +" d!pozegnanie-kanal - ustawia kanal powiadomien pożegnań\n"
                        +" d!powitanie - wiadomość powitalna\n"
                        +" d!pozegnanie - wiadomość pożegnalna\n");
                mod.setThumbnail(e.getAuthor().getEffectiveAvatarUrl());
                channel.sendMessage(mod.build()).queue();

                /*
                    INFORMACYJNE
                 */
                EmbedBuilder info = new EmbedBuilder();
                info.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734056854790864966").getAsMention()+"   Informacyjne");
                info.setColor(new Color(0, 174, 255));
                info.setFooter(" • Wiadomości wygenerowane automatycznie", e.getAuthor().getAvatarUrl());
                info.setDescription("`Komendy informacyjne` \n\n"
                        +" d!id - wyświetla nick użytkownika korzystając z ID\n"
                        +" d!server - wyświetla informacje o serwerze\n"
                        +" d!user - wyświetla informacje o użytkowniku\n"
                        +" d!autor - autorzy bota\n");
                info.setThumbnail(e.getAuthor().getEffectiveAvatarUrl());
                channel.sendMessage(info.build()).queue();

                /*
                    ZAAABAWY
                 */
                EmbedBuilder fun = new EmbedBuilder();
                fun.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734056852446380074").getAsMention()+"   Zabawy");
                fun.setColor(new Color(15, 193, 15));
                fun.setFooter(" • Wiadomości wygenerowane automatycznie", e.getAuthor().getAvatarUrl());
                fun.setDescription("`Komendy do zabawy` \n\n"
                        +" d!moneta - rzucasz monetą\n"
                        +" d!lovecalc - kalkulator miłości\n"
                        +" d!avatar - wyświetla avatar");
                fun.setThumbnail(e.getAuthor().getEffectiveAvatarUrl());
                channel.sendMessage(fun.build()).queue();

                /*
                    LEVELE
                 */
                EmbedBuilder lvl = new EmbedBuilder();
                lvl.setTitle(e.getJDA().getGuildById("720664306043191377").getEmoteById("734666494839095346").getAsMention()+"   Levele");
                lvl.setColor(new Color(255, 1, 250));
                lvl.setFooter(" • Wiadomości wygenerowane automatycznie", e.getAuthor().getAvatarUrl());
                lvl.setDescription("`Komendy do obsługi systemu leveli` \n\n"
                        +" d!level - wyświetla twój poziom");
                lvl.setThumbnail(e.getAuthor().getEffectiveAvatarUrl());
                channel.sendMessage(lvl.build()).queue();
            });

        }
        String[] args = e.getMessage().getContentRaw().split(" ", 5);
    }

}
