import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseConnectionFunctions {

    public static String getWelcomeMessage(String token, String sql) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static String getWelcomeChannel(String token, String sql) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                ps.close();
                rs.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static void updateWelcomeMessage(String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.execute();
        } catch(SQLException e) {
            System.out.println(e.toString());
        }
    }


    public static void updateWelcomeChannel(String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.execute();
        } catch(SQLException e) {
            System.out.println(e.toString());
        }
    }




    public static String getGoodbayMessage(String token, String sql) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static String getGoodbayChannel(String token, String sql) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static void updateGoodbayMessage(String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.execute();
        } catch(SQLException e) {
            System.out.println(e.toString());
        }
    }


    public static void updateGoodbayChannel(String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try {
            sql = "UPDATE ? SET goodbayChannel = ? ";
            ps = con.prepareStatement(sql);
            ps.execute();
        } catch(SQLException e) {
            System.out.println(e.toString());
        }
    }






    public static String getTokenFromTable(String ID) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT Token FROM GuildTokens WHERE GuildID = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static String getTokenFromTableTwo(String Token) {
        String tnl;
        Connection con = DbConnection.connect();
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql = "SELECT Token FROM GuildTokens WHERE Token = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, Token);
            rs = ps.executeQuery();

            tnl = rs.getString(1);

            return tnl;
        } catch(SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                rs.close();
                ps.close();;
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
        return null;
    }

    public static int getTokenFromTableThree(String Token) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select count(User) from UserLevel where User = ? ";
            ps = con.prepareStatement(sql);
            ps.setString(1, Token);
            rs = ps.executeQuery();
            int i = rs.getInt(1);
            return i;
        } catch(SQLException e) {
            System.out.println(e.toString());
            return 0;
        } finally {
            try {
                rs.close();
                ps.close();
                con.close();
            } catch(SQLException e) {
                System.out.println(e.toString());
            }
        }
    }

    public static void insertWelcomeGoodbay(String welcomeChannelID, String welcomeMessage, String goodbayChannelID, String goodbayMessage, String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try{ ps = con.prepareStatement(sql);
            ps.setString(1, welcomeMessage);
            ps.setString(2, welcomeChannelID);
            ps.setString(3, goodbayMessage);
            ps.setString(4, goodbayChannelID);
            ps.execute();

        } catch(SQLException e) {
            System.out.println(e.toString());
        }

    }

    public static void insertGuildToken(String ID, String token, String sql) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement(sql);
            ps.setString(1, ID);
            ps.setString(2, token);
            ps.execute();

        } catch(SQLException e) {
            System.out.println(e.toString());
        }

    }

    /*public static String insertToTable(String table, String data, String value) {
        Connection con = DbConnection.connect();
        PreparedStatement ps = null;
        try{
            String sql = "INSERT INTO ?(?) VALUES(?) ";
            ps = con.prepareStatement(sql);
            ps.setString(2, data);
            ps.setString(1, table);
            ps.setString(3, value);
            ps.executeQuery();

        } catch(SQLException e) {
            System.out.println(e.toString());
        }
        return null;
    }
     */

}
