import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

public class Main extends ListenerAdapter {

    public static JDA jda;

    public static void main(String[] args) throws LoginException {

        jda = new JDABuilder().setToken("NzM0NzE1NDUwOTM1NzM4NDE4.XxgT3Q.wZqUVEsXJMEsokbaZkspzzsZqP0
").build();
        jda.getPresence().setPresence(Activity.watching("DONE | Deploying..."), true);
        jda.getPresence().setStatus(OnlineStatus.DO_NOT_DISTURB);

        //TODO CUSTOM ACTIVITIES

        jda.addEventListener(new Main(), new HelpCommand(), new WelcomeGoodbay(), new Moderation(), new Fun(), new Information(), new LevelingSystem(), new RoleOnJoin());

        DbConnection.connect();
    }

    public static void sendBuildedMessageID(TextChannel ch, String emoteId, String msg) {
        MessageBuilder mb = new MessageBuilder();
        mb.appendFormat(">>> "+jda.getGuildById("720664306043191377").getEmoteById(emoteId).getAsMention()+" "+msg);
        ch.sendMessage(mb.build()).queue();
    }

    public static void sendBuildedMessageEmote(TextChannel ch, String emote, String msg) {
        MessageBuilder mb = new MessageBuilder();
        mb.appendFormat(">>> `"+emote+"` "+msg);
        ch.sendMessage(mb.build()).queue();
    }

    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        if(!e.getAuthor().isBot()) {
            if(LevelingSystem.getNumberOfUsers(e.getAuthor().getId()) == 0) {
                LevelingSystem.insertUser(e.getAuthor().getId(), 1, 100, 500);
            }
        }

    }

}
